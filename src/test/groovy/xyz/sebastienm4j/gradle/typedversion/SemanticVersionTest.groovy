package xyz.sebastienm4j.gradle.typedversion

import org.junit.Test
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersion

import static org.assertj.core.api.Assertions.assertThat

class SemanticVersionTest
{
    @Test
    void "toString return semantic version without pre release"() {
        SemanticVersion version = new SemanticVersion()
        version.setMajor(2)
        version.setMinor(0)
        version.setPatch(18)

        assertThat(version.toString()).isEqualTo("2.0.18")
    }

    @Test
    void "toString return semantic version with pre release"() {
        SemanticVersion version = new SemanticVersion()
        version.setMajor(2)
        version.setMinor(0)
        version.setPatch(18)
        version.setPreRelease("20.i.8ARF.f51657")

        assertThat(version.toString()).isEqualTo("2.0.18-20.i.8ARF.f51657")
    }
}
