package xyz.sebastienm4j.gradle.typedversion

import org.junit.Test
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersion

import static org.assertj.core.api.Assertions.assertThat

class TypedVersionPluginTest
{
    @Test
    void "raw return exactly the given version"() {
        String givenVersion = ' 1.0/2 '

        String version = TypedVersionPlugin.raw(givenVersion)

        assertThat(version).isEqualTo(version)
    }

    @Test
    void "semantic return a non-null and populated SemanticVersion"() {
        String givenVersion = '2.0.18-x.7.z.92'

        SemanticVersion version = TypedVersionPlugin.semantic(givenVersion)

        assertThat(version).isNotNull()
        assertThat(version.getMajor()).isEqualTo(2)
        assertThat(version.getMinor()).isEqualTo(0)
        assertThat(version.getPatch()).isEqualTo(18)
        assertThat(version.getPreRelease()).isEqualTo("x.7.z.92")
    }
}
