package xyz.sebastienm4j.gradle.typedversion

import org.gradle.api.Plugin
import org.gradle.api.Project
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersion
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersionParser

/**
 * The plugin entry point.
 */
class TypedVersionPlugin implements Plugin<Project>
{
    @Override
    void apply(Project target) {
        target.extensions.raw = this.&raw
        target.extensions.semantic = this.&semantic
    }

    /**
     * Return exactly the given version.
     * The version is not typed, it's simply a {@link String}.
     * You really need this plugin ? :-)
     *
     * @param version The given version
     * @return <tt>version</tt>
     */
    static String raw(String version) {
        return version
    }

    /**
     * Check and parse the given version as semantic version.
     *
     * @param version The semantic version
     * @return A typed semantic version
     */
    static SemanticVersion semantic(String version) {
        return SemanticVersionParser.parse(version)
    }
}
