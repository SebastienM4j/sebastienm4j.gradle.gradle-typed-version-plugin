Gradle Typed Version Plugin
===========================

[![Release](https://img.shields.io/badge/release-v0.1.0-blue.svg)](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/blob/master/changelog.md) [![GitLab pipeline status](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/badges/master/pipeline.svg)](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/commits/master) [![GitLab coverage report](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/badges/master/coverage.svg)](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/commits/master) [![License](https://img.shields.io/dub/l/vibe-d.svg)](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/blob/master/LICENSE)

Gradle plugin that provides typed objects to describe the version of a Gradle project.

Currently supported version schemes :

* [Semantic version](https://semver.org/)

The version is checked according to the version scheme used. It is then deserialized offering the possibility to obtain the different parts that compose it.


Installation
------------

Add the plugin in the build script :

```groovy
plugins {
  id 'xyz.sebastienm4j.gradle.typed-version'
}
```

See the [plugins gradle portal](https://plugins.gradle.org/plugin/xyz.sebastienm4j.gradle.typed-version) for more informations.


Usage
-----

### No scheme

Create a version without a specific scheme.

```groovy
version = raw('1.0')
```

The version is instance of `String`.

It's the same thing like

```groovy
version = '1.0'
```

_You really need this plugin ? :-)_

### Semantic version

Create a version that respect the [semantic version](https://semver.org/) scheme.

```groovy
version = semantic('1.0.0-RC.2')
```

_Note that build metadata is not preserved even if it is provided. It is assumed that they are determined at the build time._

The version is instance of [`SemanticVersion`](src/main/groovy/xyz/sebastienm4j/gradle/typedversion/semantic/SemanticVersion.groovy) which has several properties :

* `major`
* `minor`
* `patch`
* `preRelease`

Example :

```groovy
println "major: ${version.major}"
println "minor: ${version.minor}"
println "patch: ${version.patch}"
println "preRelease: ${version.preRelease}"
```

```shell
major: 1
minor: 0
patch: 0
preRelease: RC.2
```


License
-------

Gradle Typed Version Plugin is released under [MIT license](https://gitlab.com/SebastienM4j/gradle-typed-version-plugin/blob/master/LICENSE).
